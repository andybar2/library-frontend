import Axios from 'axios'
import { SnackbarProgrammatic as Snackbar } from 'buefy'

const Utils = {
  startLoading (ctx) {
    ctx.dispatch('loading/start', null, { root: true })
  },

  endLoading (ctx) {
    ctx.dispatch('loading/done', null, { root: true })
  },

  error (err, msg) {
    if (err.response) {
      // axios error
      console.log(err.response)

      if (err.response.data) {
        if (err.response.data.code === 3) {
          msg = 'Verify the format of the input fields.'
        } else if (err.response.data.code === 4) {
          msg = 'Resource not found.'
        } else if (err.response.data.code === 5) {
          msg = 'Resource already exists.'
        } else if (err.response.data.code === 6) {
          msg = 'Current status of the resource does not allow this action.'
        }
      }
    } else {
      console.log(err)
    }

    if (msg) {
      Snackbar.open({
        duration: 5000,
        message: msg,
        type: 'is-danger',
        position: 'is-bottom-left'
      })
    }
  },

  msg (msg) {
    Snackbar.open({
      duration: 5000,
      message: msg,
      type: 'is-warning',
      position: 'is-bottom-left'
    })
  },

  async callApi (path, params) {
    const url = process.env.BACKEND_URL + path

    const options = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    return Axios.post(url, params, options)
  }
}

export default Utils
