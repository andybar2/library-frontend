import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Buefy from 'buefy'

import 'buefy/dist/buefy.css'
import './styles/app.scss'

import LoadingStore from './store/loading.js'
import BooksStore from './store/books.js'
import EventsStore from './store/events.js'

import App from './App.vue'
import Dashboard from './pages/Dashboard.vue'
import CreateBook from './pages/CreateBook.vue'
import Book from './pages/Book.vue'
import NotFound from './pages/NotFound.vue'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(Buefy)

const store = new Vuex.Store({
  modules: {
    loading: LoadingStore,
    books: BooksStore,
    events: EventsStore
  }
})

const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'dashboard', path: '/', component: Dashboard },
    { name: 'create-book', path: '/books/new', component: CreateBook },
    { name: 'edit-book', path: '/books/:bookId', component: Book },
    { name: 'not-found', path: '*', component: NotFound }
  ]
})

/* eslint-disable-next-line no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
