import Utils from '../utils.js'

export default {
  namespaced: true,

  state: {
    data: [],
    title: '',
    page: 1
  },

  getters: {
    data (state) {
      return state.data
    },

    title (state) {
      return state.title
    },

    page (state) {
      return state.page
    }
  },

  mutations: {
    setBooks (state, books) {
      state.data = books
    },

    setTitle (state, title) {
      state.title = title
    },

    setPage (state, page) {
      state.page = page
    }
  },

  actions: {
    async load (ctx) {
      Utils.startLoading(ctx)

      try {
        const limit = 10
        const offset = (ctx.state.page - 1) * limit

        const resp = await Utils.callApi('/books/list', {
          title: ctx.state.title,
          limit,
          offset
        })

        ctx.commit('setBooks', resp.data.books)
      } catch (err) {
        Utils.error(err, 'Could not load books, try again please.')
      }

      Utils.endLoading(ctx)
    },

    async nextPage (ctx) {
      ctx.commit('setPage', ctx.state.page + 1)
    },

    async prevPage (ctx) {
      if (ctx.state.page > 1) {
        ctx.commit('setPage', ctx.state.page - 1)
      }
    },

    async updateTitle (ctx, title) {
      ctx.commit('setTitle', title)
    },

    async create (ctx, book) {
      let error = null
      Utils.startLoading(ctx)

      try {
        await Utils.callApi('/books/create', book)
      } catch (err) {
        error = err
        Utils.error(err, 'Could not create book, try again please.')
      }

      Utils.endLoading(ctx)
      return error
    },

    async read (ctx, id) {
      let book = null
      Utils.startLoading(ctx)

      try {
        const resp = await Utils.callApi('/books/read', { id })
        book = resp.data.book
      } catch (err) {
        Utils.error(err, 'Could not read book, try again please.')
      }

      Utils.endLoading(ctx)
      return book
    },

    async update (ctx, book) {
      let error = null
      Utils.startLoading(ctx)

      try {
        await Utils.callApi('/books/update', book)
      } catch (err) {
        error = err
        Utils.error(err, 'Could not update book, try again please.')
      }

      Utils.endLoading(ctx)
      return error
    },

    async delete (ctx, id) {
      let error = null
      Utils.startLoading(ctx)

      try {
        await Utils.callApi('/books/delete', { id })
      } catch (err) {
        error = err
        Utils.error(err, 'Could not delete book, try again please.')
      }

      Utils.endLoading(ctx)
      return error
    },

    async checkOut (ctx, { id, person }) {
      let error = null
      Utils.startLoading(ctx)

      try {
        await Utils.callApi('/books/events/check-out', { id, person })
      } catch (err) {
        error = err
        Utils.error(err, 'Could not check out book, try again please.')
      }

      Utils.endLoading(ctx)
      return error
    },

    async checkIn (ctx, { id, person }) {
      let error = null
      Utils.startLoading(ctx)

      try {
        await Utils.callApi('/books/events/check-in', { id, person })
      } catch (err) {
        error = err
        Utils.error(err, 'Could not check in book, try again please.')
      }

      Utils.endLoading(ctx)
      return error
    }
  }
}
