import Utils from '../utils.js'

export default {
  namespaced: true,

  state: {
    data: []
  },

  getters: {
    data (state) {
      return state.data
    }
  },

  mutations: {
    setEvents (state, events) {
      state.data = events
    }
  },

  actions: {
    async load (ctx, bookId) {
      Utils.startLoading(ctx)

      try {
        const resp = await Utils.callApi('/books/events/list', {
          id: bookId,
          limit: 10,
          offset: 0
        })

        ctx.commit('setEvents', resp.data.events)
      } catch (err) {
        Utils.error(err, 'Could not load book events, try again please.')
      }

      Utils.endLoading(ctx)
    }
  }
}
