# Library Frontend

## Install `nodejs`:

Follow the official installation instructions:
- [How to install nodejs](https://nodejs.org/)

## Clone the repository:

```bash
git clone git@gitlab.com:andybar2/library-frontend.git
cd library-frontend
```

## Install project dependencies:

```bash
npm install
```

## Run development environment:

```bash
npm start
```

## Build distribution package:

```bash
npm run build
```